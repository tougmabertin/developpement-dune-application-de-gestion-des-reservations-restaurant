# Developpement d'une application de  création de restaurants, de plats et de partage d'avis.

NoYelp est un concurrent de Yelp. Il permet de créer des restaurants, de les commenter et de créer des plats. Nous pouvons aussi rechercher des restaurants par leur nom grâce à la barre de recherche à gauche : Il devra accepter les fautes de frappes ainsi que des noms « qui se ressemble phonétiquement ».
Le projet que vous avez reàu est un projet dont tout le frontend a déjà été réalisé. Le frontend est implémenté par une autre personne et moi j'ai réalisé  partie du backend ainsi que les connections aux bases de données. 

Technologie: NodeJs, Mongodb
