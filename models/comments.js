var mongoose = require('mongoose');
var CommentSchema = new mongoose.Schema({
  message: {
    type: String,
    unique: true,
    trim: true
  },
  name: {
    type: String,
    unique: true,
    trim: true
  },
});
var Comments = mongoose.model('Comments', CommentSchema);

var stream = Comments.synchronize();
var count = 0;

stream.on('data', function(err, doc) {
    count++;
});

stream.on('close', function() {
    console.log('indexed' + count + 'documents');
});

stream.on('error', function(err) {
    console.log('Error : Crashed');
});



module.exports = User;