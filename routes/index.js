const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

/* GET home page. */
router.get('/', function (req, res, next) {
      mongoose.model('Restaurant').find({}, (err, items) => {
        if (err)
          return res.send(err);
    
          res.render('index', { title: 'Express', restaurants : items});
      });
    });

module.exports = router;
