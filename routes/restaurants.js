const express = require('express');
const router = express.Router();
const mongoose= require('mongoose');

router.get('/', (req, res) => {
        res.redirect('/');
});
// Recherche
router.post('/search', (req, res, next) => {
  mongoose.model('Restaurant').search({
    dis_max: {
      queries: [
        {
          function_score: {
            query: {
              match: {
                'name': req.query.q
              }
            },
            script_score: {
              script: '_score * 0.7'
            }
          }
        },
      ]
    }
  }, (err, items) => {
    if (err)
      return res.send(err);
    res.render('index', { restaurants: items.hits.hits.map(item => {
      const temp = item._source;
      temp.score = item._score;
      return temp;
    }) });
  });
});



router.get('/create', (req, res) => {
    res.render('restaurants/create');
});

             
router.post('/create', (req, res,next) => {
    //FAIRE CREATION
        const restaurant = req.body;
       restaurant.name = restaurant.name.split(', ');
    
      mongoose.model('Restaurant').create(restaurant,(err, item)=> {
        if (!err)
          return res.redirect('/restaurants');
        res.send(err);
      });
});

router.get('/view/:id', (req, res) =>{
    //TODO
mongoose.model('Restaurant').findById(req.params.id,(err,item)=>{
    var meals = [];
    mongoose.model('Restaurant').findById(req.params.id, function(err, item){
        mongoose.model('Meal').find({restaurantId : req.params.id}, function(err, items){
            meals = items;
            res.render('restaurants/view', { page: 'restaurant', restaurant : item , meals : meals  });
        })
    })
    
    
})
       

});

                      

router.get('/edit/:id', (req, res) => {
    // TODO

        mongoose.model('Restaurant').findById(req.params.id, function (err, item) {
            if (err)
              return res.send(err);
            const restaurant = item.toObject();
            let count = 0;
        
            if (item.name)
              item.name.forEach(names => {
                restaurant.name += names;
                if (count < item.name - 1)
                  restaurant.name += ', ';
        
                count += 1;
              })
        
            res.render('edit', { restaurant: item });
          });
        });

            

router.get('/delete/:id', (req, res) => {
   // TODO

mongoose.model('Restaurant').findByIdAndRemove(req.params.id, function (err, item) {
    if (!err)
      return res.redirect('/restaurants');
    else
       res.send(err);
  });
});

router.post('/edit/:id', (req, res) => {
   // TODO
   const restaurant=req.body;
   const name=restaurant.name.split(',',)
   mongoose.model('Restaurant').findByIdAndUpdate(req.params.id, restaurant,function(error,item){
       if(!err)
            return res.redirect('/restaurants/view/' + req.params.id);
        else
             res.send(err);
            
   });
        
});
  
module.exports = router;