const express = require('express');
const router = express.Router();
const mongoose=require('mongoose');

router.get('/create/:restaurantId', (req, res) => {
    mongoose.model('Restaurant').findById(req.params.restaurantId, function(err, item){
        res.render('meals/create', {restaurant : item});
    });
    

});
router.post('/create/:restaurantId', (req, res) => {
    let meal=req.body;
    meal.restaurantId=req.params.restaurantId;
    let allergies=[];
    let ingredients=[];
    ingredients=meal.ingredients.split(',');
    allergies=meal.allergies.split(',');
    meal.halal=meal.halal?true:false;
    meal.vegan=meal.vegan?true:false;
    meal.kosher=meal.kosher?true:false;
    
    mongoose.model('Meal').create(meal,(err,item)=>{
        if(err)
            res.send(err);
        res.redirect('/');
    })

});



module.exports = router;